import pickle
import requests
import gzip
import shutil
import csv


if __name__ == '__main__':
    f = open(r'bitstampUSD.csv.gz', "wb")
    ufr = requests.get("https://api.bitcoincharts.com/v1/csv/bitstampUSD.csv.gz", verify=False)
    f.write(ufr.content)
    f.close()
    with gzip.open('bitstampUSD.csv.gz', 'rb') as f_in:
        with open('.bitstampUSD.csv', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    candles = [[60*60*365533.0, 5.8, 5.8, 5.8, 5.8, 0]] # hour, open, high, low, close, volume
    len = 0 # we will keep len manually
    with open('.bitstampUSD.csv') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            row_hour = 60*60*(float(row[0])//(60*60))
            if row_hour == candles[len][0]: # if current tick is in existing candle
                candles[len][2] = max(candles[len][2], float(row[1]))
                candles[len][3] = min(candles[len][3], float(row[1]))
                candles[len][4] = float(row[1])
                candles[len][5] += float(row[2])
            else:
                candles.append([row_hour, float(row[1]), float(row[1]), float(row[1]), float(row[1]), float(row[2])])
                len += 1
    candles_file = open("data.pkl", "wb")
    pickle.dump(candles, candles_file)
    candles_file.close()
    pass

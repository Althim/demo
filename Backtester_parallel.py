import math
from multiprocessing import Process, Pool
import talib
import pickle
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from itertools import product
import statistics

DEBUG = False

max_cpu = 16
backtest_start_date = datetime.strptime('20100101', '%Y%m%d').date()
test_start_date = datetime.strptime('20200101', '%Y%m%d').date()
final_test_start_date = datetime.strptime('20210101', '%Y%m%d').date()

max_length = 7
spread_length = 24*7
trade_size_ptc = 20 / 100

min_bad_trades = 5


def rollingMax(a, window):
    def eachValue():
        w = a[:window].copy()
        m = w.max()
        yield m
        i = 0
        j = window
        while j < len(a):
            oldValue = w[i]
            newValue = w[i] = a[j]
            if newValue > m:
                m = newValue
            elif oldValue == m:
                m = w.max()
            yield m
            i = (i + 1) % window
            j += 1

    return np.array(list(eachValue()))


def rollingMin(a, window):
    def eachValue():
        w = a[:window].copy()
        m = w.min()
        yield m
        i = 0
        j = window
        while j < len(a):
            oldValue = w[i]
            newValue = w[i] = a[j]
            if newValue < m:
                m = newValue
            elif oldValue == m:
                m = w.min()
            yield m
            i = (i + 1) % window
            j += 1

    return np.array(list(eachValue()))


def plot_spread(spread, candles):
    plt.figure()
    plt.subplot(211)
    plt.plot(candles[:, 2])
    plt.plot(candles[:, 3])
    plt.subplot(212)
    plt.plot(spread)
    plt.show()
    pass


def shift(xs, n):
    e = np.empty_like(xs)
    if n >= 0:
        e[:n] = 0
        e[n:] = xs[:-n]
    else:
        e[n:] = 0
        e[:n] = xs[-n:]
    return e


def backtest(smas, candles, buy_length, buy_level, sell_length, sell_level, mult):
    #Calculate buy signals
    buy_sma = smas[f"buy,{buy_length}"]
    spread_sma_mult = smas[f"spread"] * mult
    buy_sma = (buy_sma * ((100 + buy_level) / 100)) - spread_sma_mult
    buy_sma = shift(buy_sma, 1)
    buy_sma = np.nan_to_num(buy_sma)
    buy_signals = buy_sma > candles[spread_length:, 3]
    # Calculate sell signals
    sell_sma = smas[f"sell,{sell_length}"]
    sell_sma = sell_sma * ((100 + sell_level) / 100)
    sell_sma = shift(sell_sma, 1)
    sell_sma = np.nan_to_num(sell_sma)
    sell_signals = sell_sma < candles[spread_length:, 2]

    money = 1000 #track base currency during trades
    money_before_trade = 0 #track base currency outside trades
    coin_amount = 0 #track secondary currency during trades
    want_to_buy = True #track if we are in trade or not
    price_for_buying = 0  #track purchase price
    good_trades = 0
    bad_trades = 0
    max_drawdown = 0
    pnl = []
    pnl_ptc = []
    #Cycle through all candles and execute trading on buy/sell signals
    for i in range(max(buy_length, sell_length), len(candles) - spread_length):
        #Keep USD amount before trade
        if want_to_buy:
            money_before_trade = money

        # Calculate drawdown when in trade
        if not want_to_buy and candles[i][3] < price_for_buying:
            drawdown = (price_for_buying - candles[i][3]) / price_for_buying * 100
            max_drawdown = max(max_drawdown, drawdown)

        #Execute BUY
        if want_to_buy and buy_signals[i]:
            price_for_buying = buy_sma[i]
            coin_amount += (money / buy_sma[i]) * 0.99925 * trade_size_ptc
            money = money * (1-trade_size_ptc)
            if DEBUG:
                print(f"Buying at {price_for_buying} of {coin_amount}")
            want_to_buy = False
        else:
            #Execute sell
            if not want_to_buy and sell_signals[i]:
                money += coin_amount * sell_sma[i] * 0.99925
                if DEBUG:
                    print(f"After selling at {sell_sma[i]} amount is {money}")
                if money > money_before_trade:
                    good_trades += 1
                else:
                    bad_trades += 1
                #Keep history
                pnl_ptc.append((money - money_before_trade)/money*100)
                pnl.append((money - money_before_trade))
                coin_amount = 0
                want_to_buy = True
    params_list = [buy_length, buy_level, sell_length, sell_level, mult]
    #Metrics
    try:
        sharpe = math.sqrt(len(pnl_ptc)) * (statistics.mean(pnl_ptc) / statistics.stdev(pnl_ptc))
        negative_pnl = [abs(ret) for ret in pnl if ret < 0]
        negative_pnl_ptc = [abs(ret) for ret in pnl_ptc if ret < 0]
        sortino = (statistics.mean(pnl_ptc) / statistics.stdev(negative_pnl_ptc))
        profit_factor = (sum(pnl)+sum(negative_pnl))/sum(negative_pnl)
    except:
        sharpe = -1
        sortino = -1
        profit_factor = -1
    return [money_before_trade, sharpe, sortino, profit_factor,
            good_trades, bad_trades, max_drawdown*trade_size_ptc,
            params_list]


def calc_smas(candles):
    # Prepare SMA and spread for backtesting
    smas = {}
    smas[f"buy,1"] = np.array(candles[:, 3])[spread_length:]
    smas[f"sell,1"] = np.array((candles[:, 3] + candles[:, 2]) / 2)[spread_length:]
    for length in range(2, max_length):
        buy_sma = talib.SMA(candles[:, 3], length)
        sell_sma = talib.SMA((candles[:, 3] + candles[:, 2]) / 2, length)
        smas[f"buy,{length}"] = buy_sma[spread_length:]
        smas[f"sell,{length}"] = sell_sma[spread_length:]

        # plot_spread(spread_sma, candles)
    spread_sma = rollingMax(candles[:, 2], spread_length) - rollingMin(candles[:, 3], spread_length)
    smas[f"spread"] = spread_sma[1:]
    return smas


def optimize_strategy():
    # Backtesting manager
    a_file = open("data.pkl", "rb")
    candles_file = pickle.load(a_file)
    a_file.close()
    candles_file = [candle for candle in candles_file
                    if test_start_date > datetime.fromtimestamp(candle[0]).date() > backtest_start_date]
    candles = np.array(candles_file)
    candles = candles.astype(float)
    smas = calc_smas(candles)
    smas_list = [smas]
    candles_list = [candles]

    buy_length = [i for i in range(1, max_length)]
    buy_level = [round(i / 5, 1) for i in range(-40, 0, 2)]
    sell_length = [i for i in range(1, max_length)]
    sell_level = [round(i / 5, 1) for i in range(-15, 10, 1)]
    mult = [round(i / 50, 2) for i in range(1, 20, 1)]

    params = product(smas_list, candles_list, buy_length, buy_level, sell_length, sell_level, mult)
    with Pool(max_cpu) as p:
        results = p.starmap(backtest, params)
    tmp_file = open(f"backtest_result_all_{str(backtest_start_date)}.pkl", "wb")
    pickle.dump(results, tmp_file)
    tmp_file.close()


def print_results(result):
    print(f'Profit: {"%.1f" % (result[0]-1000)}$ | '
          f'sharpe: {"%.1f" % (result[1])} | '
          f'sortino: {"%.1f" % (result[2])} | '
          f'profit factor {"%.1f" % (result[3])} | '
          f'Good/bad trades {result[4]}/{result[5]} | '
          f'Drawdown {"%.1f" % (result[6])} | '
          f'Params {result[7]}')


def backtest_single_param(result):
    a_file = open("data.pkl", "rb")
    candles_file = pickle.load(a_file)
    a_file.close()
    candles_file = [candle for candle in candles_file
                    if test_start_date > datetime.fromtimestamp(candle[0]).date() > backtest_start_date]
    candles = np.array(candles_file)
    candles = candles.astype(float)
    smas = calc_smas(candles)
    backtest_result = backtest(smas, candles, result[7][0], result[7][1], result[7][2], result[7][3], result[7][4])
    print(f"Backtest results {str(backtest_start_date)}-20200101")
    print_results(backtest_result)


def test_strategy(result):
    a_file = open("data.pkl", "rb")
    candles_file = pickle.load(a_file)
    a_file.close()
    candles_file = [candle for candle in candles_file
                    if final_test_start_date > datetime.fromtimestamp(candle[0]).date() > test_start_date]
    candles = np.array(candles_file)
    candles = candles.astype(float)
    smas = calc_smas(candles)
    backtest_result = backtest(smas, candles, result[7][0], result[7][1], result[7][2], result[7][3],  result[7][4])
    print("Test results 20200101-20210101")
    print_results(backtest_result)


def final_test_strategy(result):
    a_file = open("data.pkl", "rb")
    candles_file = pickle.load(a_file)
    a_file.close()
    candles_file = [candle for candle in candles_file
                    if datetime.fromtimestamp(candle[0]).date() > final_test_start_date]
    candles = np.array(candles_file)
    candles = candles.astype(float)
    smas = calc_smas(candles)
    backtest_result = backtest(smas, candles, result[7][0], result[7][1], result[7][2], result[7][3],  result[7][4])
    print("Final results 20210101-...")
    print_results(backtest_result)


def tests(result):
    backtest_single_param(result)
    test_strategy(result)
    print("--------------")


def print_stats():
    a_file = open(f"backtest_result_all_{str(backtest_start_date)}.pkl", "rb")
    results_all = pickle.load(a_file)
    a_file.close()
    max_profit = results_all[0]
    max_sharpe = results_all[0]
    max_sortino = results_all[0]
    max_profit_factor = results_all[0]
    for result in results_all:
        if result[0] > max_profit[0] and result[5] > min_bad_trades:
            max_profit = result
        if result[1] > max_sharpe[1] and result[5] > min_bad_trades:
            max_sharpe = result
        if result[2] > max_sortino[2] and result[5] > min_bad_trades:
            max_sortino = result
        if result[3] > max_profit_factor[3] and result[5] > min_bad_trades:
            max_profit_factor = result
    print("Test max_profit")
    tests(max_profit)
    print("Test max_sharpe")
    tests(max_sharpe)
    print("Test max_sortino")
    tests(max_sortino)
    print("Test max_profit_factor")
    tests(max_profit_factor)
    print("Final test - max profit")
    final_test_strategy(max_profit)

if __name__ == '__main__':
    #optimize_strategy()
    print_stats()
    pass
